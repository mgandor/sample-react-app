import React from 'react';
import Plot from 'react-plotly.js';

const PieChart = ({ data }) => (
  <Plot
    data={[
      {
        values: data,
        type: 'pie',
      },
    ]}
    layout={ {width: 500, height: 500, title: 'A Fancy Plot'} }
  />
);


export default PieChart