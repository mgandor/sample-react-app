import { useEffect, useState } from 'react';
import './App.css';
import PieChart from './components/PieChart';

function App() {
  const [pieChartState, setPieChartStateState] = useState(null);
  const [timeState, setTimeStateState] = useState(null);

  useEffect(() => {
    // fetch('http://example.com/movies.json')
    // .then(response => response.json())
    // .then(data => setTimeStateState(data));

    setPieChartStateState([1, 2, 3]);
    setTimeStateState([10, 2, 45])
  }, []);

  return (
    <div className="App">
      hallo
      <div>
        <PieChart data={pieChartState} />
      </div>
      <div>
        <PieChart data={timeState} />
      </div>
    </div>
  );
}

export default App;
